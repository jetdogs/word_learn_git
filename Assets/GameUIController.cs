﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PanelId
{
    
    Unknown = -1,
    StartingGamePanel,
    GamePlayPanel,
    LoadingPanel,
    ResultsPanel,
    GroupSelectionPanel,
    GamePlayLettersPanel,
    GamePlayBlitzPanel,

}
public class GameUIController : _Singleton<GameUIController>
{
    PanelId previousPanel;
  
    public GameObject[] uiPanels;
    // Start is called before the first frame update
    public void InitializeUI()
    {
        previousPanel = PanelId.Unknown;

        for(int i = 0; i < uiPanels.Length; i++)
        {
            uiPanels[i].SetActive(false);
        }
    }

    public GameObject GetObject(PanelId id, string name)
    {
        return uiPanels[(int)id].transform.Find(name).gameObject;
    }

    public GameObject GetObject(string name)
    {
        return uiPanels[(int)previousPanel].transform.Find(name).gameObject;
    }

    public void ShowPanel(PanelId id)
    {
        Debug.Log("ShowPanel: " + id);

        if(previousPanel != PanelId.Unknown)
            uiPanels[(int)previousPanel].SetActive(false);

        
        uiPanels[(int)id].SetActive(true);

        previousPanel = id;
    }

    public void HidePanel(PanelId id)
    {
        if(id == PanelId.Unknown && previousPanel != PanelId.Unknown)
            uiPanels[(int)previousPanel].SetActive(false);
        else
             uiPanels[(int)id].SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
