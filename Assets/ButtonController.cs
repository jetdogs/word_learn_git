﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ButtonType
{
    StartButton,
    WordButton,
    NextQuestionButton,
    SelectGroup,
    GroupButton,
    SwitchLanguagePairButton,
    StartLetterTestButton,
    CancelWholeTestButton,
    SkipQuestionButton,
    StartBlitzButton,
}

[RequireComponent(typeof(Button))]
public class ButtonController : MonoBehaviour
{
    public ButtonType buttonType;
    public Button button;
    public int id = 0;
    public char ch = '\0';

    // Start is called before the first frame update
    void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(OnButtonClicked);
    }

    void OnButtonClicked()
    {
        Debug.Log($"Clicked: {buttonType} id: {id}");

        GameState.GetInstance().ButtonPress(buttonType,id);

        switch (buttonType)
        {
           /* case ButtonType.ReplayButton:
                break;

            case ButtonType.PlayButton:
                GameState.GetInstance().StartLevel();
                break;
            
            case ButtonType.NextButton:
                GameState.GetInstance().NextLevel();
                break;*/

            
        }
    }
}
