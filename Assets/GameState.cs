﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public enum State
{

    FirstScreen,
    GamePlay,
    GamePlayWithLetters,
    GamePlayWordsBlitz
}

public struct Word
{
    public Dictionary<String, String> data;
}

public class WordList
{
    public List<Word> list;

    public WordList()
    {
        list = new List<Word>();
    }
}

public class GameState : _Singleton<GameState>
{
    // Start is called before the first frame update
    int gameStep = 0;
    int maxWordsInTest = 20;
    int wordsInTest = 10;
    int currentWord = 0;
    int[] wordsForCurrentTest;
    int[] wordsOnButtons;
    bool[] testResult;
    string testGroup;
    List<string> wordsGroups = new List<string>();
    string[] langPair = { "ru", "fi" };

    char[] wordLetters;
    List<GameObject> wordLetterButtons = new List<GameObject>();
    List<char> answerLetters = new List<char>();


    Coroutine hintCoroutine = null;
    int hintButtonId = -1;
    bool hintDisable = false;
    bool hintUsed = false;
    float hintTime = 3;
    float defaultHintTime = 3;
    //--------------

    public GameObject winParticles;
    State state;
    WordList wordList = new WordList();

    IEnumerator GetText()
    {
        Debug.Log("Loading web data...");
        var googleId = "1hLek24GFrdH_t9uODvi_z3dAhmreL2Az9p1ARNqSY5k";
        string s = string.Format("https://docs.google.com/spreadsheets/d/{0}/export?format=csv&id={0}&gid=0", googleId);

        UnityWebRequest www = UnityWebRequest.Get(s);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Show results as text
            Debug.Log(www.downloadHandler.text);

            var lines = www.downloadHandler.text.Split('\n');
            string[] keys = null;

            for (int i = 0; i < lines.Length; i++)
            {
                var line = lines[i].Trim();

                //var values = line.Split(',');
                string[] values = Regex.Split(line, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");

                if (i == 0)
                {
                    keys = values;
                    continue;
                }

                Word w = new Word();
                w.data = new Dictionary<string, string>();

                for (int j = 0; j < values.Length; j++)
                {
                    Debug.Log($"{i} - {j} - {values[1]}");
                    if(keys[j] == "ru-group")
                    {
                        if (wordsGroups.IndexOf(values[j]) < 0)
                            wordsGroups.Add(values[j]);
                    }

                    keys[j] = keys[j].ToLower().Replace(".", "");

                    values[j] = values[j].ToLower().Replace(".","").Replace("\"","");


                    w.data.Add(keys[j], values[j]);
                }

                wordList.list.Add(w);
            }


            GameUIController.GetInstance().ShowPanel(PanelId.StartingGamePanel);

            UpdateLanguagePairButton();

            GameObject textObj = GameUIController.GetInstance().GetObject(PanelId.StartingGamePanel, "Text");
            textObj.GetComponent<TextMeshProUGUI>().text = $"Dictionary has {wordList.list.Count} words in {wordsGroups.Count} group!";

            Debug.Log($"Loaded {lines.Length - 1} lines");

            // Or retrieve results as binary data
            byte[] results = www.downloadHandler.data;
        }
    }


    public static void Randomize<T>(T[] items)
    {
        for (int i = 0; i < items.Length - 1; i++)
        {
            int j = Random.Range(i, items.Length);
            T temp = items[i];
            items[i] = items[j];
            items[j] = temp;
        }
    }

    public static void Swap<T>(T[] items, int i, int j)
    {
        T temp = items[i];
        items[i] = items[j];
        items[j] = temp;
    }

    void Start()
    {
        gameStep = 0;
        testGroup = "";

        GameUIController.GetInstance().InitializeUI();

        GameUIController.GetInstance().ShowPanel(PanelId.LoadingPanel);

        // winParticles.SetActive(false);

        state = State.FirstScreen;

        StartCoroutine(GetText());
        //--



    }

    // Update is called once per frame
    void Update()
    {

    }

    public void StartWordsChecking(State s)
    {
      
        state = s;

        if(state == State.GamePlay)
            GameUIController.GetInstance().ShowPanel(PanelId.GamePlayPanel);
        else if (state == State.GamePlayWithLetters)
            GameUIController.GetInstance().ShowPanel(PanelId.GamePlayLettersPanel);
        else
            GameUIController.GetInstance().ShowPanel(PanelId.GamePlayBlitzPanel);


        gameStep = 0;

        //select words for current test
        //TODO: use groups
        //TODO: select mainly poorly learned words

        wordsInTest = maxWordsInTest;

        testResult = new bool[maxWordsInTest];

        wordsForCurrentTest = new int[maxWordsInTest];


        List<int> wordsTestList = new List<int>();

        for(int j = 0; j < wordList.list.Count; j++)
        {
            var w = wordList.list[j];

            if (testGroup.Length == 0 || w.data["ru-group"] == testGroup)
                wordsTestList.Add(j);
        }

        while (wordsTestList.Count > maxWordsInTest)
        {
            wordsTestList.RemoveAt(Random.Range(0, wordsTestList.Count));
        }


        wordsForCurrentTest = wordsTestList.ToArray();

        wordsInTest = wordsTestList.Count;

        Randomize(wordsForCurrentTest);

        if (s == State.GamePlay)
            PrepareCurrentStep();
        else if (s == State.GamePlayWithLetters)
            PrepareCurrentLettersStep();
        else
            PrepareCurrentBlitzStep();
    }

    public void PrepareCurrentStep()
    {
        GameObject wordObj = GameUIController.GetInstance().GetObject(PanelId.GamePlayPanel, "WordQuestion");

        GameObject[] wordButton =
            {
                GameUIController.GetInstance().GetObject(PanelId.GamePlayPanel, "GroupLayout/WordButton0"),
                GameUIController.GetInstance().GetObject(PanelId.GamePlayPanel, "GroupLayout/WordButton1"),
                GameUIController.GetInstance().GetObject(PanelId.GamePlayPanel, "GroupLayout/WordButton2")
            };

        GameObject textObj = GameUIController.GetInstance().GetObject(PanelId.GamePlayPanel, "ProgressText");
        textObj.GetComponent<TextMeshProUGUI>().text = $"{gameStep+1}/{wordsInTest}";


        //select one necessary word
        //randomly select others 
        //shuffle them
        //check progress
        //select only from specific group

        currentWord = wordsForCurrentTest[gameStep];

        wordObj.GetComponentInChildren<TextMeshProUGUI>().text = wordList.list[currentWord].data[langPair[0]];
                
        int wordsCount = 3;
        wordsOnButtons = new int[wordsCount];

        wordsOnButtons[0] = currentWord;

        //TODO: select words from the same group
        for (int i = 1; i < 3;)
        {
            int newWord = Random.Range(0, wordList.list.Count);

            if (Array.IndexOf(wordsOnButtons, newWord) < 0)
            {
                wordsOnButtons[i] = newWord;
                i++;
            }
        }

        Randomize(wordsOnButtons);


        for (int i = 0; i < 3; i++)
        {
            wordButton[i].GetComponentInChildren<TextMeshProUGUI>().text = wordList.list[wordsOnButtons[i]].data[langPair[1]];
        }
    }

    public string GenerateDummyString(int len)
    {
        string res = "";

        for(int i = 0; i < len; i++)
        {
            if (i < answerLetters.Count)
                res += answerLetters[i];
            else
                res += "_";

            if (i != len - 1)
                res += " ";
        }

        return res;
    }

    public void CheckPressedWord(int id)
    {
        //check word
        GameUIController.GetInstance().ShowPanel(PanelId.ResultsPanel);

        GameObject bgObj = GameUIController.GetInstance().GetObject(PanelId.ResultsPanel, "Background");
        GameObject answerObj = GameUIController.GetInstance().GetObject(PanelId.ResultsPanel, "Answer");

        bool result = (wordsOnButtons[id] == currentWord);

        ShowResultPanel(result, 
            wordList.list[currentWord].data[langPair[1]],
            wordList.list[wordsOnButtons[id]].data[langPair[1]]);

    }

    public void PrepareCurrentLettersStep()
    {
        foreach (var b in wordLetterButtons)
            GameObject.Destroy(b);

        wordLetterButtons.Clear();
        answerLetters.Clear();

        currentWord = wordsForCurrentTest[gameStep];

        GameObject textObj = GameUIController.GetInstance().GetObject("ProgressText");
        textObj.GetComponent<TextMeshProUGUI>().text = $"{gameStep + 1}/{wordsInTest}";


        GameObject wordObj = GameUIController.GetInstance().GetObject("WordQuestion");
        wordObj.GetComponentInChildren<TextMeshProUGUI>().text = wordList.list[currentWord].data[langPair[0]];

        var questionWord = wordList.list[currentWord].data[langPair[1]];

        GameObject answerObj = GameUIController.GetInstance().GetObject("WordAnswer");
        GameObject layoutGroupObj = GameUIController.GetInstance().GetObject("GroupLayout");
        GameObject letterButtonObj = layoutGroupObj.transform.Find("WordButton0").gameObject;

        wordLetters = questionWord.ToCharArray();

        Randomize(wordLetters);

        string dummyAnswer = GenerateDummyString(wordLetters.Length);
        answerObj.GetComponentInChildren<TextMeshProUGUI>().text = dummyAnswer;

        

        //clone letters 
        for (int i = 0; i < wordLetters.Length; i++)
        {
            var o = GameObject.Instantiate(letterButtonObj, layoutGroupObj.transform);

            o.SetActive(true);

            var b = o.GetComponent<ButtonController>();

            b.id = i;
            b.ch = wordLetters[i];

            o.GetComponentInChildren<TextMeshProUGUI>().text = wordLetters[i].ToString();

            wordLetterButtons.Add(o);
        }

        letterButtonObj.SetActive(false);

        CreateHintTimer(true);

    }

    public void PrepareCurrentBlitzStep()
    {
        GameObject wordObj = GameUIController.GetInstance().GetObject(PanelId.GamePlayBlitzPanel, "WordQuestion");
        GameObject wordAnswObj = GameUIController.GetInstance().GetObject(PanelId.GamePlayBlitzPanel, "WordAnswer");

        GameObject textObj = GameUIController.GetInstance().GetObject(PanelId.GamePlayBlitzPanel, "ProgressText");
        textObj.GetComponent<TextMeshProUGUI>().text = $"{gameStep + 1}/{wordsInTest}";

        currentWord = wordsForCurrentTest[gameStep];

        string questionWord = wordList.list[currentWord].data[langPair[0]];
        string answerWord;

        if (Random.Range(0, 2) == 0)
            answerWord = wordList.list[currentWord].data[langPair[1]];
        else
            //answerWord = wordList.list[Random.Range(0,wordList.list.Count)].data[langPair[1]];
            answerWord = GetCloseMatchWordTo(wordList.list[currentWord].data[langPair[1]], wordList.list[currentWord].data[langPair[0]]);

        wordObj.GetComponentInChildren<TextMeshProUGUI>().text = questionWord;

        wordAnswObj.GetComponentInChildren<TextMeshProUGUI>().text = answerWord;
    }

    string GetCloseMatchWordTo(string text, string qWord)
    {
        string res = "";
        int dist = (1 << 30) - 1;

        foreach(var s in wordList.list)
        {
            string ss = s.data[langPair[1]];

            if (text == ss)
                continue;

            if (testGroup != "" && s.data["ru-group"] != testGroup)
                continue;

            if (s.data[langPair[0]] == qWord)
                continue;

            int d = LevenshteinDistance(ss,text);

            if(d < dist)
            {
                dist = d;
                res = s.data[langPair[1]];
            }
        }

        return res;
    }

    public void CheckPressedBlitzAnswer(int id)
    {
        GameObject wordAnswObj = GameUIController.GetInstance().GetObject(PanelId.GamePlayBlitzPanel, "WordAnswer");
        string answerWords = wordAnswObj.GetComponentInChildren<TextMeshProUGUI>().text;

        //check word
        GameUIController.GetInstance().ShowPanel(PanelId.ResultsPanel);

        //GameObject bgObj = GameUIController.GetInstance().GetObject(PanelId.ResultsPanel, "Background");
        

        string questionWordTranslate = wordList.list[currentWord].data[langPair[1]];
        

        bool result = (questionWordTranslate == answerWords) == (id == 1);

        ShowResultPanel(result,
            wordList.list[currentWord].data[langPair[1]],
            wordList.list[currentWord].data[langPair[0]]);
    }

    public void CreateHintTimer(bool enableHints)
    {
        if (enableHints)
        {
            hintDisable = false;
            hintTime = defaultHintTime;
            //StopHint();
        }

        //StopHint();

        if (hintDisable)
            return;

        if (hintUsed)
            hintTime *= 0.9f;
        else
            hintTime *= 1.1f;

        hintUsed = false;

        string currentWordString = wordList.list[currentWord].data[langPair[1]];

        //restore color for previous hint
        if(hintButtonId >= 0)
        {
            var prevButton = wordLetterButtons[hintButtonId].GetComponent<Button>();

            if(prevButton.interactable)
            {
                wordLetterButtons[hintButtonId].GetComponent<Image>().color = Color.white;
            }
        }

        //find new hint
        hintCoroutine = StartCoroutine(HintCoroutine());

        foreach(var bObj in wordLetterButtons)
        {
            var b = bObj.GetComponent<ButtonController>();

            if (b.ch == currentWordString[answerLetters.Count] && b.button.interactable)
            {
                hintButtonId = b.id;
                break;
            }
        }
    }

    public void StopHint()
    { 
        if (hintCoroutine != null)
        {
            hintButtonId = -1;

            StopCoroutine(hintCoroutine);

            hintCoroutine = null;
        }
    }


    public IEnumerator HintCoroutine()
    {
        yield return new WaitForSeconds(hintTime);

        //if(hintButtonId > 0)
        wordLetterButtons[hintButtonId].GetComponent<Image>().color = Color.yellow;

        hintUsed = true;

        StopHint();
    }

    public void CheckPressedLetter(int id)
    {
        string currentWordString = wordList.list[currentWord].data[langPair[1]];

        char letter = wordLetters[id];

        answerLetters.Add(letter);
        
        string dummyAnswer = GenerateDummyString(wordLetters.Length);
        GameObject answerObj = GameUIController.GetInstance().GetObject("WordAnswer");
        answerObj.GetComponentInChildren<TextMeshProUGUI>().text = dummyAnswer;

        var letterButton = wordLetterButtons[id].GetComponent<Button>();
        letterButton.interactable = false;

        //Debug.Log($"{currentWordString} {answerLetters.Count-1} {currentWordString[answerLetters.Count - 1]} {letter}");

        StopHint();

        //letter is wrong
        if (currentWordString[answerLetters.Count - 1] != letter)
        {
            hintDisable = true;
        }

        wordLetterButtons[id].GetComponent<Image>().color = currentWordString[answerLetters.Count - 1] != letter ? Color.red : Color.green;

        if(answerLetters.Count == wordLetters.Length)
        {
            var a = new string(answerLetters.ToArray());
            var b = currentWordString; 

            bool result = (a == b);

            ShowResultPanel(result, a, b);

            //ShowResultPanel(false, "", wordList.list[currentWord].data[langPair[1]]);
        }
        else
        {
            CreateHintTimer(false);
        }

    }

    void ShowResultPanel(bool result, string enteredWord, string correctWord)
    {
        testResult[gameStep] = result;

        GameUIController.GetInstance().ShowPanel(PanelId.ResultsPanel);

        GameObject bgObj = GameUIController.GetInstance().GetObject(PanelId.ResultsPanel, "Background");
        GameObject answerObj2 = GameUIController.GetInstance().GetObject(PanelId.ResultsPanel, "Answer");
        GameObject textObj = GameUIController.GetInstance().GetObject(PanelId.ResultsPanel, "Header/Text");

        textObj.GetComponent<TextMeshProUGUI>().text = enteredWord;

        //win
        if (result)
        {
            bgObj.GetComponent<Image>().color = new Color(0.66f, 1, 0.66f);
            answerObj2.GetComponent<TextMeshProUGUI>().text = "Correct!";
        }
        else
        {
            bgObj.GetComponent<Image>().color = new Color(1, 0.66f, 0.66f);
            answerObj2.GetComponent<TextMeshProUGUI>().text = "Wrong!\nCorrect is\n" + correctWord;
        }

        //gameStep++;
    }
    

    internal void OpenGroupsPanel()
    {
        GameUIController.GetInstance().ShowPanel(PanelId.GroupSelectionPanel);

        GameObject layoutObj = GameUIController.GetInstance().GetObject(PanelId.GroupSelectionPanel, "Scroll View/Viewport/Content");

        GameObject button = layoutObj.transform.Find("Button").gameObject;

        button.GetComponentInChildren<TextMeshProUGUI>().text = "none";
        button.GetComponent<ButtonController>().id = -1;

        for (int i = 0; i < wordsGroups.Count; i++)
        {
            var b = GameObject.Instantiate(button, layoutObj.transform);
            b.GetComponentInChildren<TextMeshProUGUI>().text = wordsGroups[i];
            b.GetComponent<ButtonController>().id = i;
        }

        //button.SetActive(false);
    }

    void UpdateGroup(int id)
    {
        GameUIController.GetInstance().ShowPanel(PanelId.StartingGamePanel);

        GameObject groupButton = GameUIController.GetInstance().GetObject(PanelId.StartingGamePanel, "GroupButton");

        if (id < -1)
            return;
        
        testGroup = (id == -1) ? "" : wordsGroups[id];

        groupButton.GetComponentInChildren<TextMeshProUGUI>().text = "Group: " + ((testGroup == "") ? "none" : testGroup); 
    }

    void UpdateLanguagePairButton()
    {
        GameObject pairButton = GameUIController.GetInstance().GetObject(PanelId.StartingGamePanel, "LanguagePairButton");

        pairButton.GetComponentInChildren<TextMeshProUGUI>().text = langPair[0] + " -> " + langPair[1];
    }


    internal void ButtonPress(ButtonType buttonType, int id = 0)
    {
        switch (buttonType)
        {
            case ButtonType.StartButton:

                StartWordsChecking(State.GamePlay);
                break;

            case ButtonType.StartLetterTestButton:

                StartWordsChecking(State.GamePlayWithLetters);

                break;


            case ButtonType.StartBlitzButton:

                StartWordsChecking(State.GamePlayWordsBlitz);

                break;

            case ButtonType.WordButton:

                if (state == State.GamePlay)
                    CheckPressedWord(id);
                else if (state == State.GamePlayWithLetters)
                    CheckPressedLetter(id);
                else
                    CheckPressedBlitzAnswer(id);

                break;

            case ButtonType.CancelWholeTestButton:
                StopHint();

                GameUIController.GetInstance().ShowPanel(PanelId.StartingGamePanel);
                break;

            case ButtonType.SkipQuestionButton:

                ShowResultPanel(false, "", wordList.list[currentWord].data[langPair[1]]);
                break;

            case ButtonType.NextQuestionButton:

                gameStep++;

                StopHint();

                if (gameStep >= wordsInTest)
                {
                    GameUIController.GetInstance().ShowPanel(PanelId.StartingGamePanel);
                }
                else
                {
                    if (state == State.GamePlay)
                    {
                        GameUIController.GetInstance().ShowPanel(PanelId.GamePlayPanel);
                        PrepareCurrentStep();
                    }
                    else if(state == State.GamePlayWithLetters)
                    {
                        GameUIController.GetInstance().ShowPanel(PanelId.GamePlayLettersPanel);
                        PrepareCurrentLettersStep();
                    }
                    else if (state == State.GamePlayWordsBlitz)
                    {
                        GameUIController.GetInstance().ShowPanel(PanelId.GamePlayBlitzPanel);
                        PrepareCurrentBlitzStep();
                    }
                }

                break;

            case ButtonType.SelectGroup:
         
                OpenGroupsPanel();

                break;

            case ButtonType.GroupButton:

                UpdateGroup(id);

                break;

            case ButtonType.SwitchLanguagePairButton:

                Swap(langPair,0,1);

                UpdateLanguagePairButton();

                break;

          
        }
    }

    public static int LevenshteinDistance(string s, string t)
    {
        int n = s.Length;
        int m = t.Length;
        int[,] d = new int[n + 1, m + 1];
        if (n == 0)
        {
            return m;
        }
        if (m == 0)
        {
            return n;
        }
        for (int i = 0; i <= n; d[i, 0] = i++)
            ;
        for (int j = 0; j <= m; d[0, j] = j++)
            ;
        for (int i = 1; i <= n; i++)
        {
            for (int j = 1; j <= m; j++)
            {
                int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;
                d[i, j] = Math.Min(
                    Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                    d[i - 1, j - 1] + cost);
            }
        }
        return d[n, m];
    }


}
